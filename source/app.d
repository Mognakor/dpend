import std.stdio;

import dpend.api.annotations.Qualifier;
import dpend.api.annotations.UseScope;
import dpend.api.context.context;
import dpend.api.mixins.managed;

@UseScope( "prototype")
class Asdf
{
    mixin Managed;

    static int cnt = 0;

    Qwerty qwerty;
    int num;

    this(Qwerty qwerty)
    {
        this.qwerty = qwerty;
        this.num = ++cnt;
    }
}

class Qwerty
{
    import dpend.api.annotations.Inject;

    mixin Managed;

    this()
    {
        writeln( "Qwerty::this");
    }

    @Inject
    void inject1(Qwerty param)
    {

    }

    @Inject
    private void inject2()
    {

    }
}

void main()
{
    import dpend.core.registry.registry;

    writeln( scanInfoRegistry);

    writeln( "Edit source/app.d to start your project.");

    ApplicationContext ctx = ApplicationContext( "");

    writeln( scanInfoRegistry["app.Qwerty"]);

    Qwerty qwerty = ctx.getInstance!Qwerty;

    Qwerty qwerty2 = ctx.getInstance!Qwerty;

    Asdf asdf = ctx.getInstance!Asdf;
    Asdf asdf2 = ctx.getInstance!Asdf;

    writeln( asdf.num);
    writeln( asdf2.num);
}
