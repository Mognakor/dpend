module dpend.util.ensureconstructor;

mixin template ensureconstructor()
{
    static if( __traits( hasMember, typeof(this), "__ctor" ) == false)
    {
        this()
        {
        }
    }
}
