module dpend.util.functioncall;

template GenerateCall(string func, funcParams...)
{
    string makeSingle(size_t n, string type)() {
        return "args[" ~ n.stringof ~ "].get!" ~ funcParams[n].stringof;
    }

    string make(size_t n)()
    {
        static if ( (n + 1) == funcParams.length)
            return makeSingle!(n, funcParams[n].stringof);
        else static if (n < funcParams.length)
            return makeSingle!(n, funcParams[n].stringof) ~ ", " ~ make!(n+1)();
        else
            return "";
    }

    enum argStr = make!0();

    const char[] GenerateCall = func ~ "(" ~ argStr ~ ")";
}

template GenReturnCall(string prefix, string func, funcParams...)
{
    const char[] GenReturnCall = "return Variant(" ~prefix ~ GenerateCall!(func, funcParams) ~ ");";
}
