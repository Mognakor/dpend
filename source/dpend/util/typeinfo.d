module dpend.util.typeinfo;

string fullTypeName(alias T)()
{
    static if( is( T ==  class) )
    {
        // workaround since classinfo.name does not work at compile-time
        enum typeIdStr = typeof( T.init ).classinfo.stringof;
        return typeIdStr[8 .. $-1];
    }
    else static if( is( T == struct ) )
    {
        return fullTypeName!(__traits(parent, T))  ~ "." ~ T.stringof;
    }
    else static if( __traits( isModule, T ) )
    {
        return fullTypeName!(__traits(parent, T)) ~ "." ~ T.stringof[7.. $];
    }
    else static if( __traits( isPackage, T ) )
    {
        static if(hasParent!T)
        {
            return fullTypeName!(__traits(parent, T)) ~ "." ~ T.stringof[8.. $];
        }
        else
        {
            return T.stringof[8.. $];
        }
    }
    else
    {
        return T.stringof;
    }
}

template hasAttribute(alias T, string name, U)
{
    alias member = __traits( getMember, T, name);
    alias attributes = __traits( getAttributes, member);

    static if (attributes.length == 0)
    {
        enum hasAttribute = false;
    }

    static foreach(i, attr; attributes)
    {
        static if ( fullTypeName!attr == fullTypeName!U )
        {
            enum hasAttribute = true;
        }
        else static if ( i + 1 == attributes.length )
        {
            enum hasAttribute = false;
        }

    }
}

template hasParent(alias T)
{ 
    enum hasParent = __traits(compiles, __traits( parent, T) );
} 

