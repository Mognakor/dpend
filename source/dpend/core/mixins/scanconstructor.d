module dpend.core.mixins.scanconstructor;

string getScope(alias T)()
{
    import dpend.api.annotations.UseScope;

    enum attributes = __traits(getAttributes, T);

    static if(attributes.length == 0)
    {
        return "singleton";
    }

    static foreach( i, attr ; attributes)
    {
        static if( is( typeof(attr) == UseScope ) )
        {
            UseScope useScope = attr;

            return useScope.scopeName;
        }
        else static if ( i + 1 == attributes.length )
        {
            return "singleton";
        }
    }
}

mixin template scanConstructor(alias T)
{
    import std.traits : Parameters, ReturnType;
    import std.variant;

    import dpend.core.info.scaninfo;
    import dpend.core.mixins.functionParameters;

    import dpend.util.functioncall;
    import dpend.util.typeinfo;

    enum scopeName = getScope!T;

    alias ctor = __traits(getMember, T, "__ctor");
    enum retType = typeid(ReturnType!( ctor ));
    alias funcParams = Parameters!ctor;

    enum funcParamInfo = functionParameters!( ctor );

    enum name = fullTypeName!T;
    
    enum Variant function(Variant[]) instantiator = (Variant[] args) {
        mixin( GenReturnCall!("new ", T.stringof, funcParams) );
    };

    enum scanConstructor = ProviderInfo(retType, name, instantiator, funcParamInfo, scopeName);
}

template scanPostConstruct(alias T)
{
    pragma(msg, T);

    enum allMembers = __traits(allMembers, T);

    InjectionPoint[] makeSingle(string name)()
    {
        import std.traits : isFunction, Parameters;
        import std.variant;

        import dpend.api.annotations.Inject;
        import dpend.core.mixins.functionParameters;
        import dpend.util.functioncall;
        import dpend.util.typeinfo;

        enum hasAttr = hasAttribute!(T, name, Inject);
        static if(!hasAttr)
        {
            return [];
        }
        else
        {
            alias member = __traits(getMember, T, name);
            static if( "public" != __traits(getProtection, member) )
            {
                pragma(msg, "WARN: Symbols marked with @Inject must be public. Symbol: ", name, " has visibility ",
                    __traits(getProtection, member));
                return [];
            } else {
                static if (isFunction!member) {
                    alias funcParams = Parameters!member;

                    enum funcParamInfo = functionParameters!member;

                    enum void function(Object target, Variant[]) injector = (Object target, Variant[] args) {
                        mixin( "(cast(" ~ T.stringof ~ ")target)." ~ GenerateCall!(name, funcParams) ~ ";" );
                    };

                    return [InjectionPoint(name, injector, funcParamInfo)];
                } else {
                    // TODO
                    return [];
                }
            }

        }

    }

    InjectionPoint[] make(size_t n)()
    {
        static if ( (n + 1) == allMembers.length)
        {
            enum name = allMembers[n];
            return makeSingle!(name);
        }
        else static if (n < allMembers.length)
        {
            enum name = allMembers[n];
            return makeSingle!(name) ~  make!(n+1)();
        }
        else
            return [];
    }

    enum argStr = make!0();

    pragma(msg, argStr);

    enum scanPostConstruct = PostConstructInfo.init;
} 


mixin template scanConstruction(alias T)
{
    static if ( __traits(hasMember, T, "__ctor") )
    {
        import std.typecons : Nullable;

        mixin scanConstructor!T;

        mixin scanPostConstruct!T;

        enum scanConstruction = Nullable!ConstructionInfo( ConstructionInfo( scanConstructor, scanPostConstruct ) );
    }
    else
    {
        // TODO
         enum scanConstruction = Nullable!ConstructionInfo.init;
    }
}
