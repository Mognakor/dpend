module dpend.core.mixins.functionParameters;

template functionParameters( alias func )
{
    import dpend.core.info.scaninfo;
    import dpend.util.typeinfo;

    static if( is( typeof(func) funcParams == __parameters) )
    {
        template fnParams()
        {
            template functionParameter(size_t varIndex)
            {
                alias sliced = funcParams[varIndex .. varIndex + 1];
                alias slicedAttr = __traits( getAttributes, sliced );

                enum typeName = fullTypeName!(sliced[0]);

                enum name = __traits(identifier, sliced);

                enum qualifierStr = qualifier!( slicedAttr );

                enum functionParameter = ParameterInfo(typeName, qualifierStr, name);
            }

            ParameterInfo[] make(size_t n)()
            {
                static if (n < funcParams.length)
                    return functionParameter!n ~ make!(n+1)();
                else
                {
                    ParameterInfo[0] arr = [];
                    return [];
                }
            }

            enum fnParams = make!0();
        }

        enum functionParameters = fnParams!();
    }
    else
    {
        static assert(0, "arguments do not name a function");
    }
}

template qualifier( slicedAttr... )
{
    import dpend.api.annotations.Qualifier;

    string make(size_t n)()
    {
        static if (n < slicedAttr.length)
        {
            enum attr = slicedAttr[n];
            static if( is( typeof(attr) == Qualifier ) )
            {
                Qualifier qualifier = attr;
                return qualifier.name;
            }
            else
            {
                return make!(n+1)();
            }
        }
        else
            return "";
    }

    static if( slicedAttr.length == 0 )
    {
        enum qualifier = "";
    }
    else
    {
        enum qualifier = make!0();
    }
}
