module dpend.core.info.scaninfo;

// Information about a scanned type
struct ScanInfo
{
    import std.typecons : Nullable;

    // information about the constructor
    Nullable!ConstructionInfo constructionInfo;
    // list of provided beans, empty if none
    ProviderInfo[] providerInfos;
}

// information for a constructor call, consists of information on how to call the constructor and post construction information
struct ConstructionInfo
{
    ProviderInfo constructorInfo;
    PostConstructInfo postConstructInfo;

    this(ProviderInfo constructorInfo, PostConstructInfo postConstructInfo)
    {
        this.constructorInfo = constructorInfo;
        this.postConstructInfo = postConstructInfo;
    }
}

// Information about a bean provider, either a constructor or a function
struct ProviderInfo
{
    import std.variant;

    // name of the provider, will use the full type name in case of constructors
    string name;
    // scope of the created instance, e.g. prototype or singleton, set to singleton if none given
    string instanceScope;
    // return type
    TypeInfo type;
    // function to call to construct an instance
    Variant function(Variant[]) instantiator;
    // parameters for the instantiator
    ParameterInfo[] parameterInfo;

    this(TypeInfo type, string name, Variant function(Variant[]) instantiator, ParameterInfo[] parameterInfo, string instanceScope) {
        this.type = type;
        this.name = name;
        this.instantiator = instantiator;
        this.parameterInfo = parameterInfo;
        this.instanceScope = instanceScope;
    }

    // all types and interfaces implemented by the return type
    string[] implementedTypes()
    {
        // TODO
        return [typeid(type).name];
    }
}

// Actions to be taken after bean has been constructed, e.g. calling setters
struct PostConstructInfo
{
    InjectionPoint[] injectionPoints;
}

// bean member annotated with Inject (currently only functions supported), can be used to solve cyclic dependencies
struct InjectionPoint
{
    import std.variant;

    // name of the member
    string name;
    // generated function
    void function(Object target, Variant[]) injector;
    // information about the parameters of injector
    ParameterInfo[] parameterInfo;

    this(string name, void function(Object target, Variant[]) injector, ParameterInfo[] parameterInfo)
    {
        this.name = name;
        this.injector = injector;
        this.parameterInfo = parameterInfo;
    }
}


// Information about a parameter
struct ParameterInfo
{
    // full type name e.g. packagea.packageb.module.type
    string fullType;
    // value of Qualifier annotation to specify a bean by name
    string qualifier;
    // identifer name
    string name;

    this(string fullType, string qualifier, string name)
    {
        this.fullType = fullType;
        this.qualifier = qualifier;
        this.name = name;
    }
}
