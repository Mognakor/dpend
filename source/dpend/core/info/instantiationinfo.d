module dpend.core.info.instantiationinfo;

struct InstantiatorInfo
{
    import std.variant;

    import dpend.core.info.scaninfo;

    string instanceScope;

    string className;
    string name;
    ParameterInfo[] params;
    Variant function(Variant[]) instantiator;

    this(string className, string name, ParameterInfo[] params, Variant function(Variant[]) instantiator, string instanceScope)
    {
        this.className = className;
        this.name = name;
        this.params = params;
        this.instantiator = instantiator;
        this.instanceScope = instanceScope;
    }

}