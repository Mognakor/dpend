module dpend.api.context.context;

import std.stdio;
import std.variant;

struct ApplicationContext
{
    import dpend.api.customization.InstanceScope;

    import dpend.core.info.instantiationinfo;
    import dpend.core.info.scaninfo;
    import dpend.core.registry.registry;

    private const InstantiatorInfo[string] contextClasses;
    private InstanceScope[string] scopes;

    this(string contextPath)
    {
        this.contextClasses = constructInstantiatorInfo();
        this.scopes = constructScopes();
    }

    static InstantiatorInfo[string] constructInstantiatorInfo()
    {
        InstantiatorInfo[string] contextClasses;
        foreach(string className; scanInfoRegistry.keys)
        {
            ScanInfo scanInfo = scanInfoRegistry[className];

            ConstructionInfo constructionInfo = scanInfo.constructionInfo.get();
            ProviderInfo constructorInfo = constructionInfo.constructorInfo;

            writeln(constructorInfo.type);

            contextClasses[className] = InstantiatorInfo(className, constructorInfo.name, constructorInfo.parameterInfo, constructorInfo.instantiator, constructorInfo.instanceScope);
        }

        return contextClasses;
    }

    static InstanceScope[string] constructScopes()
    {
        InstanceScope[string] scopes;

        scopes["prototype"] = new PrototypeScope;
        scopes["singleton"] = new SingletonScope;

        return scopes;
    }
    public T getInstance(T)()
    {
        string className = T.classinfo.name;

        bool[string] visitedClasses;

        Variant var = getInstance(className, visitedClasses);

        return var.get!T();
    }

    private Variant getInstance(string className, bool[string] visitedClasses)
    {
        const InstantiatorInfo* classInfo = (className in this.contextClasses);
        if(classInfo == null) {
            throw new Exception("Could not find a managed instance of type " ~ className);
        }

        InstanceScope instanceScope = this.scopes[classInfo.instanceScope];

        return instanceScope.get(classInfo.name, constructInstance(className, visitedClasses, classInfo) );
    }

    private Variant constructInstance(string className, bool[string] visitedClasses, const InstantiatorInfo* classInfo)
    {
        const bool visited = visitedClasses.get(className, false);
        if(visited)
        {
            string treeMembers = "[";
            foreach(string key; visitedClasses.keys)
            {
                treeMembers = treeMembers ~ key ~ ", ";
            }
            treeMembers = treeMembers ~ "]";

            throw new Exception("Class " ~ className ~ " already is present in dependency tree. Tree: " ~ treeMembers);
        }

        visitedClasses[className] = true;

        const ParameterInfo[] paramInfos = classInfo.params;

        Variant[] arguments;

        foreach(ParameterInfo info; paramInfos)
        {
            arguments = arguments ~ getInstance(info.fullType, visitedClasses);
        }

        visitedClasses.remove(className);

        return classInfo.instantiator(arguments);
    }

}
