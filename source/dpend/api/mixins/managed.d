module dpend.api.mixins.managed;

mixin template Managed(string name = null)
{
    // TODO TMP Debug imports
    import std.stdio;

    // actual imports
    import dpend.util.ensureconstructor;

    mixin ensureconstructor;

    shared static this()
    {
        import dpend.core.info.scaninfo;
        import dpend.core.mixins.scanconstructor;
        import dpend.core.registry.registry;

        import dpend.util.typeinfo;

        alias type = typeof(this);

        enum typeName = fullTypeName!type;

        writeln("Component begin: " ~ typeName);

        ScanInfo scanInfo;

        mixin scanConstruction!type;
        scanInfo.constructionInfo = scanConstruction;

        writeln(scanInfo);

        scanInfoRegistry[typeName] = scanInfo;
    }
}
