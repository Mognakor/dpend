module dpend.api.customization.InstanceScope;

import std.variant;

interface InstanceScope {
    Variant get(string name, lazy Variant instanceProvider);
}

class PrototypeScope : InstanceScope {
    Variant get(string name, lazy Variant instanceProvider) {
        return instanceProvider();
    }
}

class SingletonScope : InstanceScope {
    Variant[string] singletons;

    Variant get(string name, lazy Variant instanceProvider) {
        synchronized {
            Variant* instance = (name in this.singletons);

            if(instance == null)
            {
                singletons[name] = instanceProvider;
            }

            return singletons[name];
        }
    }
}
